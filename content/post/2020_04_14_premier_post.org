{{ $optOrg := dict "markup" "org" }}

#+title: LABASE et COVID-19
#+subtitle: Mise en place d'un protocole de test pour savoir si vos masques DIY sont efficaces face au COVID-19
#+date: 2020-04-14
#+tags[]: Template Hugo OrgMode
#+draft: false
#+author: 

*Mise en place d'un protocole de test pour savoir si vos masques DIY sont efficaces face au COVID-19*